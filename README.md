# hm-news

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### 打开的本地地址:http://localhost:8888

### 可以在 `vue.config.js` 中修改启动的地址和是否自动给打开浏览器

### 项目中使用了 vuex 来保存 keep-alive 的参数,用来做新闻列表页的状态保持

## 黑马头条的导航守卫

### 在 vue-router 的文件中,在 router 导出之前设置 `router.beforeEach(function(to,from,next){})` 在这里判断用户从何来,到哪去,以及进行 next 放行

## 项目结构

### 项目中 components 文件夹下放入自己封装的公用组件

### 项目中 pages 文件夹下放的是也 main 级别的文件

### assect 文件夹下放静态资源文件

### public 文件夹下放入网页图标和有挂载点的网页

### vue.config.js 中可以设置跨域请求

###
