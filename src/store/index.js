import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    cacheList: []
  },
  mutations: {
    add(state, payload) {
      if (!state.cacheList.includes(payload.name)) {
        state.cacheList.push(payload.name)
      }
    },
    del(state, payload) {
      if (state.cacheList.includes(payload.name)) {
        state.cacheList = state.cacheList.filter(item => item != payload.name)
      }
    }
  }
})

export default store
