import Vue from 'vue'
import App from './App.vue'
//导入公共样式
import './styles/base.less'
//导入字体图标
import './styles/iconfont.less'
//导入自动设置屏幕适配
import 'lib-flexible'
//导入路由
import router from './router/index'
import HmHeader from './components/HmHeader.vue'
import HmLogo from './components/HmLogo.vue'
import HmButton from './components/HmButton.vue'
import HmInput from './components/HmInput.vue'
import HmNavbar from './components/HmNavbar.vue'
import HmDialog from './components/HmDialog.vue'
import HmFocus from './components/HmFocus.vue'
import HmCollect from './components/HmCollect.vue'
import HmWonder from './components/HmWonder.vue'

//--------------------------导入vant-ui-------------------------------------------------------
import { button } from 'vant'
Vue.use(button)
import { Toast } from 'vant'
Vue.use(Toast)
import { Dialog } from 'vant'
Vue.use(Dialog)
import { Field } from 'vant'
Vue.use(Field)
import {
  RadioGroup,
  Radio,
  Cell,
  CellGroup,
  List,
  Tab,
  Tabs,
  PullRefresh
} from 'vant'
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(Cell)
Vue.use(CellGroup)
Vue.use(List)
Vue.use(Tab)
Vue.use(Tabs)
import { Uploader } from 'vant'
Vue.use(Uploader)
Vue.use(PullRefresh)

Vue.config.productionTip = false

Vue.component('hm-header', HmHeader)
Vue.component('hm-logo', HmLogo)
Vue.component('hm-button', HmButton)
Vue.component('hm-input', HmInput)
Vue.component('hm-navbar', HmNavbar)
Vue.component('hm-dialog', HmDialog)
Vue.component('hm-focus', HmFocus)
Vue.component('hm-collect', HmCollect)
Vue.component('hm-wonder', HmWonder)

//----------------------------------------------------------------------
//导入axios
import axios from 'axios'
axios.defaults.baseURL = 'http://localhost:3000'
//设置响应过滤器
axios.interceptors.response.use(res => {
  if (res.data.statusCode == 401 && res.data.message == '用户信息验证失败') {
    router.push({
      name: 'login',
      params: {
        back: true
      }
    })
    Toast.fail(res.data.message)
    localStorage.removeItem('user_id')
    localStorage.removeItem('token')
  }
  return res
})
//设置请求过滤器
axios.interceptors.request.use(config => {
  const token = localStorage.getItem('token')

  if (token) {
    config.headers.Authorization = token
  }

  return config
})
//把axios添加到Vue的原型上
Vue.prototype.$axios = axios

//------------------------定义全局过滤器-----------------------------
import moment from 'moment'
Vue.filter('date', function(input, format = 'YYYY-MM-DD') {
  return moment(input).format(format)
})
Vue.filter('date2', function(input) {
  const now = new Date()
  const time = new Date(input)

  const gettime = (now - time) / 1000 / 60 //分钟的数据
  if (gettime < 1) {
    return `刚刚`
  } else if (gettime < 60) {
    return `${gettime | 0}分钟前`
  } else if (gettime > 60 && gettime < 60 * 24) {
    return moment(input).format('HH:MM.SS')
  } else {
    return moment(input).format('YYYY-MM-DD')
  }
})

//导入vuex
import store from './store'

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
