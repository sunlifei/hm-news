import vue from 'vue'
import vuerouter from 'vue-router'
import login from '../pages/login.vue'
import register from '../pages/register.vue'
import user from '../pages/user.vue'
import focus from '../pages/focus.vue'
import collect from '../pages/collect.vue'
import follow from '../pages/follow.vue'
import editinfo from '../pages/editinfo.vue'
import home from '../pages/home.vue'
import search from '../pages/search.vue'
import category from '../pages/category.vue'
import postinfo from '../pages/postinfo.vue'
import store from '../store'

vue.use(vuerouter)

const router = new vuerouter({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/register',
      component: register,
      name: 'register'
    },
    {
      path: '/user',
      component: user,
      name: 'user'
    },
    {
      path: '/focus',
      component: focus,
      name: 'focus'
    },
    {
      path: '/collect',
      component: collect,
      name: 'collect'
    },
    {
      path: '/follow',
      component: follow,
      name: 'follow'
    },
    {
      path: '/editinfo',
      component: editinfo,
      name: 'editinfo'
    },
    {
      path: '/home',
      name: 'home',
      component: home
    },
    {
      path: '/search',
      name: 'search',
      component: search
    },
    {
      path: '/category',
      name: 'category',
      component: category
    },
    {
      path: '/postinfo',
      name: 'postinfo',
      component: postinfo
    }
  ]
})

//在路由导出之前设置导航守卫

//在路由导航之前
const urls = ['/user', '/editinfo']
router.beforeEach(function(to, from, next) {
  if (to.name == 'home') {
    store.commit('add', {
      name: 'home'
    })
  }

  const token = localStorage.getItem('token')

  if (urls.includes(to.path)) {
    if (token) {
      next()
    } else {
      router.push({
        name: 'login',
        params: {
          back: true
        }
      })
    }
  } else {
    next()
  }
})

//在路由导航之后

export default router
