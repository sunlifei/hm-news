module.exports = {
  devServer: {
    //设置自动打开浏览器
    open: true,
    //设置端口号
    port: 8888
  },
  //设置px自动转换成rem
  css: {
    loaderOptions: {
      css: {},
      postcss: {
        plugins: [
          require('postcss-px2rem')({
            remUnit: 36 //根据设计图设计为他的0.1倍
          })
        ]
      }
    }
  }
}
